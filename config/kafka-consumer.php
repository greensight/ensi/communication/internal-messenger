<?php

use App\Domain\Kafka\Actions\Listen\ListenCreateMessageAction;
use Ensi\LaravelInitialEventPropagation\RdKafkaConsumerMiddleware;
use Ensi\LaravelMetrics\Kafka\KafkaMetricsMiddleware;

return [
    'global_middleware' => [RdKafkaConsumerMiddleware::class, KafkaMetricsMiddleware::class,],
    'stop_signals' => [SIGTERM, SIGINT],

    'processors' => [
        [
            'topic' => 'create-message',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenCreateMessageAction::class,
            'queue' => false,
        ],
    ],
    'consumer_options' => [
        'default' => [
            'consume_timeout' => 5000,
            'middleware' => [],
        ],
    ],
];
