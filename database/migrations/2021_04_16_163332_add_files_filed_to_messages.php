<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->jsonb('files')->nullable();
        });
        Schema::dropIfExists('attachments');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->dropColumn('files');
        });

        Schema::create('attachments', function (Blueprint $table) {
            $table->id();
            $table->integer('message_id');
            $table->string('name');
            $table->string('path');
            $table->string('url');
            $table->timestamps();
            $table->foreign('message_id')->references('id')->on('messages');
        });
    }
};
