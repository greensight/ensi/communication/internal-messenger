<?php

namespace App\Domain\Messages\Models;

use App\Http\ApiV1\OpenApiGenerated\Enums\ChatDirectionEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\UserTypeEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Chat
 *
 * @property int $id
 * @property ChatDirectionEnum $direction
 * @property string|null $theme
 * @property int|null $type_id
 * @property bool $muted
 * @property int $user_id
 * @property bool $unread_user
 * @property bool $unread_admin
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 * @property UserTypeEnum $user_type
 *
 * @property-read Collection<Message> $messages
 */
class Chat extends Model
{
    protected $table = 'chats';

    public const FILLABLE = [
        'direction',
        'theme',
        'type_id',
        'muted',
        'user_id',
        'unread_user',
        'unread_admin',
        'user_type',
    ];

    protected $fillable = self::FILLABLE;

    protected $casts = [
        'direction' => ChatDirectionEnum::class,
        'user_type' => UserTypeEnum::class,
    ];

    public function messages(): HasMany
    {
        return $this->hasMany(Message::class, 'chat_id', 'id');
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($chat) {
            foreach ($chat->messages as $message) {
                $message->delete();
            }
        });
    }
}
