<?php

namespace App\Domain\Messages\Models;

use Carbon\CarbonInterface;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $path
 * @property string $disk
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 */
class Attachment extends Model
{
    protected static function boot(): void
    {
        parent::boot();

        self::deleted(function (self $attachment) {
            if ($attachment->path) {
                $filesystem = resolve(EnsiFilesystemManager::class);
                $disk = $attachment->disk === 'protected' ? $filesystem->protected() : $filesystem->public();
                $disk->delete($attachment->path);
            }
        });
    }
}
