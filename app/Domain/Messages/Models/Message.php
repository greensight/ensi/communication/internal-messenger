<?php

namespace App\Domain\Messages\Models;

use App\Http\ApiV1\OpenApiGenerated\Enums\UserTypeEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Message
 * @package App\Domain\Messages\Models
 *
 * @property int $id
 * @property int $chat_id
 * @property int $user_id
 * @property string $text
 * @property array $files
 * @property UserTypeEnum $user_type
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read Chat $chat
 */
class Message extends Model
{
    protected $table = 'messages';

    protected $casts = [
        'files' => 'array',
        'user_type' => UserTypeEnum::class,
    ];

    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class, 'chat_id', 'id');
    }
}
