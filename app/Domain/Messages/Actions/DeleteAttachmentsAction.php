<?php

namespace App\Domain\Messages\Actions;

use App\Domain\Messages\Models\Attachment;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;

class DeleteAttachmentsAction
{
    public function __construct(protected EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(array $attachmentIds): void
    {
        $attachments = Attachment::query()->whereIn('id', $attachmentIds)->get();
        $paths = $attachments->pluck('path')->toArray();
        $this->fileManager->delete($paths);
        foreach ($attachments as $attachment) {
            $attachment->delete();
        }
    }
}
