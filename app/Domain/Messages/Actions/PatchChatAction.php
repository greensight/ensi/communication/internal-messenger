<?php

namespace App\Domain\Messages\Actions;

use App\Domain\Messages\Models\Chat;

class PatchChatAction
{
    public function execute(int $id, array $fields): Chat
    {
        /** @var Chat $chat */
        $chat = Chat::query()->findOrFail($id);
        $chat->update($fields);

        return $chat;
    }
}
