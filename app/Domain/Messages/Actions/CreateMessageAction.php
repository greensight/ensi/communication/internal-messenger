<?php

namespace App\Domain\Messages\Actions;

use App\Domain\Messages\Actions\Data\MessageData;
use App\Domain\Messages\Models\Chat;
use App\Domain\Messages\Models\Message;
use App\Exceptions\MessageException;

class CreateMessageAction
{
    /**
     * @throws MessageException
     */
    public function execute(MessageData $messageData): Message
    {
        $chatExists = Chat::query()->where('id', $messageData->chatId)->exists();

        if (!$chatExists) {
            $message = "An error occurred while saving the message: chat id=$messageData->chatId not found";

            throw new MessageException($message);
        }

        $message = new Message();
        $message->chat_id = $messageData->chatId;
        $message->text = $messageData->text;
        $message->user_id = $messageData->userId;
        $message->user_type = $messageData->userType;
        $message->files = $messageData->files;
        $message->save();

        return $message;
    }
}
