<?php

namespace App\Domain\Messages\Actions\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\UserTypeEnum;

class MessageData
{
    public function __construct(
        public int $chatId,
        public string $text,
        public int $userId,
        public UserTypeEnum $userType,
        public array $files = []
    ) {
    }
}
