<?php

namespace App\Domain\Messages\Actions;

use App\Domain\Messages\Models\Chat;

class CreateChatAction
{
    public function execute(array $fields): Chat
    {
        $chat = new Chat();
        $chat->fill($fields);
        $chat->save();

        return $chat;
    }
}
