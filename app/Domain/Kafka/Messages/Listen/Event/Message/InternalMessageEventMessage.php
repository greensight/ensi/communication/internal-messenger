<?php

namespace App\Domain\Kafka\Messages\Listen\Event\Message;

use RdKafka\Message;

class InternalMessageEventMessage
{
    public function __construct(public InternalMessagePayload $attributes)
    {
    }

    public static function makeFromRdKafka(Message $message): static
    {
        $payload = json_decode($message->payload, true);

        return new static(new InternalMessagePayload($payload));
    }
}
