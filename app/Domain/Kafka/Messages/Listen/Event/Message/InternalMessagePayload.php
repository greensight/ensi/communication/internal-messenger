<?php

namespace App\Domain\Kafka\Messages\Listen\Event\Message;

use App\Domain\Kafka\Messages\Listen\Event\Payload;

/**
 * @property string $text
 * @property int $chat_id
 * @property int $user_id
 * @property int $user_type
 * @property ?array $files
 */
class InternalMessagePayload extends Payload
{
    protected bool $timestamps = false;
}
