<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\Event\Message\InternalMessageEventMessage;
use App\Domain\Messages\Actions\CreateMessageAction;
use App\Domain\Messages\Actions\Data\MessageData;
use App\Exceptions\MessageException;
use App\Http\ApiV1\OpenApiGenerated\Enums\UserTypeEnum;
use RdKafka\Message;

class ListenCreateMessageAction
{
    public function __construct(protected CreateMessageAction $createMessageAction)
    {
    }

    /**
     * @throws MessageException
     */
    public function execute(Message $kafkaMessage): void
    {
        $message = InternalMessageEventMessage::makeFromRdKafka($kafkaMessage);
        $payload = $message->attributes;

        $this->createMessageAction->execute(new MessageData(
            chatId: $payload->chat_id,
            text: $payload->text,
            userId: $payload->user_id,
            userType: UserTypeEnum::from($payload->user_type),
            files: $payload->files
        ));
    }
}
