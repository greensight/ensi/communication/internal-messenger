<?php

namespace App\Http\ApiV1\Modules\Messages\Controllers;

use App\Domain\Messages\Actions\CreateChatAction;
use App\Domain\Messages\Actions\DeleteChatAction;
use App\Domain\Messages\Actions\PatchChatAction;
use App\Http\ApiV1\Modules\Messages\Queries\ChatsQuery;
use App\Http\ApiV1\Modules\Messages\Requests\CreateChatRequest;
use App\Http\ApiV1\Modules\Messages\Requests\PatchChatRequest;
use App\Http\ApiV1\Modules\Messages\Resources\ChatsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class ChatsController
{
    public function create(CreateChatRequest $request, CreateChatAction $action): Responsable
    {
        return new ChatsResource($action->execute($request->validated()));
    }

    public function patch(int $chatId, PatchChatRequest $request, PatchChatAction $action): Responsable
    {
        return new ChatsResource($action->execute($chatId, $request->validated()));
    }

    public function delete(int $chatId, DeleteChatAction $action): Responsable
    {
        $action->execute($chatId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, ChatsQuery $query): Responsable
    {
        return ChatsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
