<?php

namespace App\Http\ApiV1\Modules\Messages\Controllers;

use App\Domain\Messages\Actions\CreateAttachmentAction;
use App\Domain\Messages\Actions\DeleteAttachmentsAction;
use App\Http\ApiV1\Modules\Messages\Requests\CreateAttachmentRequest;
use App\Http\ApiV1\Modules\Messages\Requests\MassDeleteAttachmentsRequest;
use App\Http\ApiV1\Modules\Messages\Resources\AttachmentsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class AttachmentsController
{
    public function create(CreateAttachmentRequest $request, CreateAttachmentAction $action): Responsable
    {
        return new AttachmentsResource($action->execute($request->getFile(), $request->getName()));
    }

    public function massDelete(MassDeleteAttachmentsRequest $request, DeleteAttachmentsAction $action): Responsable
    {
        $action->execute($request->getIds());

        return new EmptyResource();
    }
}
