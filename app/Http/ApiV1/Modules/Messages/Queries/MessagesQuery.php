<?php

namespace App\Http\ApiV1\Modules\Messages\Queries;

use App\Domain\Messages\Models\Message;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\ExtraFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class MessagesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Message::query());

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('chat_id'),
            AllowedFilter::exact('user_id'),

            ...ExtraFilter::nested('chat', [
                AllowedFilter::exact('type_id'),
                AllowedFilter::exact('unread_admin'),
                ...StringFilter::make('theme')->contain(),
            ]),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('id');
    }
}
