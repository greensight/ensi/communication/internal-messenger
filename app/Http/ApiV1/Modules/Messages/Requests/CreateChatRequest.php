<?php

namespace App\Http\ApiV1\Modules\Messages\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\ChatDirectionEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\UserTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class CreateChatRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'direction' => ['required', new Enum(ChatDirectionEnum::class)],
            'theme' => ['required', 'string'],
            'type_id' => ['required', 'integer'],
            'muted' => ['required', 'boolean'],
            'user_id' => ['required', 'integer'],
            'unread_user' => ['nullable', 'boolean'],
            'unread_admin' => ['nullable', 'boolean'],
            'user_type' => ['required', new Enum(UserTypeEnum::class)],
        ];
    }
}
