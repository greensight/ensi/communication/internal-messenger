<?php

namespace App\Http\ApiV1\Modules\Messages\Requests;

use App\Http\ApiV1\Support\Requests\MassDeleteRequest;

class MassDeleteAttachmentsRequest extends MassDeleteRequest
{
}
