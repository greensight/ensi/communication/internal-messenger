<?php

namespace App\Http\ApiV1\Modules\Messages\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\UserTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class PatchChatRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'theme' => ['string'],
            'type_id' => ['integer'],
            'muted' => ['boolean'],
            'unread_user' => ['boolean'],
            'unread_admin' => ['boolean'],
            'user_type' => ['required', new Enum(UserTypeEnum::class)],
        ];
    }
}
