<?php

namespace App\Http\ApiV1\Modules\Messages\Resources;

use App\Domain\Messages\Models\Chat;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Chat */
class ChatsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'direction' => $this->direction,
            'theme' => $this->theme,
            'type_id' => $this->type_id,
            'muted' => $this->muted,
            'user_id' => $this->user_id,
            'user_type' => $this->user_type,
            'unread_user' => $this->unread_user,
            'unread_admin' => $this->unread_admin,
            'messages' => MessagesResource::collection($this->whenLoaded('messages')),
        ];
    }
}
