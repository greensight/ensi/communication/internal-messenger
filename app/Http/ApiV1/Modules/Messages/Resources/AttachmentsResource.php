<?php

namespace App\Http\ApiV1\Modules\Messages\Resources;

use App\Domain\Messages\Models\Attachment;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Attachment */
class AttachmentsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'path' => $this->path,
            'disk' => $this->disk,
            'url' => $this->mapPublicFileToResponse($this->path),
        ];
    }
}
