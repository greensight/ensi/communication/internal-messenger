<?php

namespace App\Http\ApiV1\Support\Requests;

class MassDeleteRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'ids' => ['required', 'array'],
            'ids.*' => ['integer'],
        ];
    }

    /** @return int[] */
    public function getIds(): array
    {
        return $this->input('ids');
    }
}
